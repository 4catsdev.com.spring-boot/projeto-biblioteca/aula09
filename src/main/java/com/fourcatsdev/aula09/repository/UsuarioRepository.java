package com.fourcatsdev.aula09.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.aula09.modelo.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
}
