package com.fourcatsdev.aula09.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.aula09.modelo.Papel;

public interface PapelRepository extends JpaRepository<Papel, Long> {
	Papel findByPapel(String papel);
}
